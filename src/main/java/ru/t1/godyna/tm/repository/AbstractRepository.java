package ru.t1.godyna.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.api.repository.IRepository;
import ru.t1.godyna.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository <M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final List<M> records = new ArrayList<>();

    @Nullable
    @Override
    public M add(@Nullable final M model) {
        records.add(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOne(@Nullable final M model) {
        records.remove(model);
        return model;
    }

    @Override
    public void removeAll(@Nullable Collection<M> collection) {
        if (collection == null) return;
        records.removeAll(collection);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return records;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        @NotNull final List<M> result = new ArrayList<>(records);
        result.sort(comparator);
        return result;
    }

    @Override
    public void removeAll() {
        records.clear();
    }
    
    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        return records
                .stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst().orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        return records.get(index);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public M removeOneById(@Nullable final String id) {
        @Nullable final M model = findOneById(id);
        removeOne(model);
        return model;
    }

    @Nullable
    @Override
    public M removeOneByIndex(@Nullable final Integer index) {
        @Nullable final M model = findOneByIndex(index);
        removeOne(model);
        return model;
    }

    @NotNull
    @Override
    public Integer getSize() {
        return records.size();
    }

}
